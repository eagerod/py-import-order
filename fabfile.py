import os

from fabric.api import local, task, runs_once


PROJECT_ROOT_DIRECTORY = os.path.dirname(__file__)
PROJECT_TESTS_DIRECTORY = os.path.join(PROJECT_ROOT_DIRECTORY, 'tests')
PROJECT_SOURCES_ROOT_DIRECTORY = os.path.join(PROJECT_ROOT_DIRECTORY, 'src')

PROJECT_MAIN_SCRIPT_PATH = os.path.join(PROJECT_SOURCES_ROOT_DIRECTORY, 'import_orderer.py')


@task
@runs_once
def setup(quiet=False):
    requirements_file = os.path.join(PROJECT_ROOT_DIRECTORY, 'requirements.txt')
    local('pip install {} -r {}'.format('--quiet' if quiet else '', requirements_file))

    project_hooks_directory = os.path.join(PROJECT_ROOT_DIRECTORY, 'hooks')
    git_hooks_directory = os.path.join(PROJECT_ROOT_DIRECTORY, '.git', 'hooks')
    local('ln -sf {}/* {}/'.format(project_hooks_directory, git_hooks_directory))


@task
@runs_once
def lint():
    setup(quiet=True)
    local('flake8 {}'.format(PROJECT_ROOT_DIRECTORY))
    local('python {0} -s {1} {1}'.format(PROJECT_MAIN_SCRIPT_PATH, PROJECT_SOURCES_ROOT_DIRECTORY))


@task
@runs_once
def test():
    setup(quiet=True)
    local('python -m unittest discover -v -t {} -s {}'.format(PROJECT_ROOT_DIRECTORY, PROJECT_TESTS_DIRECTORY))


@task
@runs_once
def install_globally(extras=''):
    if extras:
        extras = '[{}]'.format(extras)
    local('pip install --upgrade {}{}'.format(PROJECT_ROOT_DIRECTORY, extras))
