import setuptools


setuptools.setup(
    name='import_helper',
    license='MIT',
    version='0.1.0',
    description='get sorted Python import blocks.',
    author='Aleem Haji',
    author_email='hajial@gmail.com',
    packages=['import_helper'],
    package_dir={'import_helper': 'src'},
    entry_points={
        'console_scripts': [
            'py_order = import_helper.import_orderer:do_program'
        ]
    }
)
