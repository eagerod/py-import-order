import argparse
import imp
import os
import re
import sys


IMPORT_STATEMENT_RE = r'^import\s+[\w\.]+|^from\s+[\w\.]+\s+import\s+\w+(?:,\s+\w+)*(?:,\s+\\)?'
PACKAGE_RE = r'^(?:import\s+([\w\.]+))|^(?:from\s+([\w\.]+))'

_, OS_IMPORT_PATH, _ = imp.find_module('os')
PYTHON_LIB_IMPORT_DIRECTORY = os.path.dirname(OS_IMPORT_PATH)


class ImportSection(object):
    SYSTEM_PACKAGE = 1
    INSTALLED_PACKAGE = 2
    PROJECT_PACKAGE = 3


class ImportType(object):
    IMPORT = 'import'
    FROM = 'from'


class ImportStatement(object):
    def __init__(self, line, source_dir, start=None):
        self.text = line.strip()
        self.source_dir = source_dir
        self.start = start

        self._category = None
        self._package_name = None
        self._import_type = None

        matches = re.findall(PACKAGE_RE, self.text)
        if matches[0][0]:
            self._import_type = ImportType.IMPORT
            self._package_name = matches[0][0]
        elif matches[0][1]:
            self._import_type = ImportType.FROM
            self._package_name = matches[0][1]
        else:
            raise Exception('Cannot find module name in {}'.format(self.text))

    @property
    def category(self):
        if self._category is not None:
            return self._category

        if self._package_name in sys.builtin_module_names:
            return ImportSection.SYSTEM_PACKAGE

        # Try to import increasingly less specific modules from this package until we find one that's actually a
        # package.
        # 1. Check the directory the file is in
        # 2. Check the start directory, since that's likely to be sys.path[0]
        # 3. Just try importing without any extra pathing.
        fp, pathname, stuff = self._try_import_module(self._package_name, [self.source_dir])
        if stuff:
            return ImportSection.PROJECT_PACKAGE

        fp, pathname, stuff = self._try_import_module(self._package_name, [self.start])
        if stuff:
            return ImportSection.PROJECT_PACKAGE

        fp, pathname, stuff = self._try_import_module(self._package_name)
        if not stuff:
            raise Exception('failed to find module: {}'.format(self.text))

        if pathname.startswith(PYTHON_LIB_IMPORT_DIRECTORY):
            return ImportSection.SYSTEM_PACKAGE

        suffix, mode, type = stuff
        if type == imp.PY_SOURCE or type == imp.C_EXTENSION:
            return ImportSection.SYSTEM_PACKAGE
        elif type == imp.PKG_DIRECTORY:
            return ImportSection.INSTALLED_PACKAGE

        print >> sys.stderr, (suffix, mode, type)
        raise AssertionError('unknown import source from {}'.format(self.text))

    def _try_import_module(self, module_name, extras=()):
        while module_name:
            try:
                if extras:
                    return imp.find_module(module_name, extras)
                return imp.find_module(module_name)
            except ImportError:
                module_name = '.'.join(module_name.split('.')[:-1])
        return None, None, None


class ImportOrderer(object):
    def __init__(self, fh, start=None):
        self.file = fh
        self.start = start
        self.import_statements = []
        self.statement_sections = {
            ImportSection.SYSTEM_PACKAGE: [],
            ImportSection.INSTALLED_PACKAGE: [],
            ImportSection.PROJECT_PACKAGE: []
        }

    def read_file(self):
        line = self.get_line()
        while line:
            self.readline(line)
            line = self.get_line()

    def get_line(self):
        line = self.file.readline()
        while line.strip().endswith('\\'):
            line = '{}{}'.format(line, self.file.readline())
        return line

    def readline(self, line):
        if re.match(IMPORT_STATEMENT_RE, line):
            self.add_import_line(line)

    def add_import_line(self, statement):
        self.import_statements.append(ImportStatement(statement, os.path.dirname(self.file.name), start=self.start))

    def categorize_statements(self):
        for import_statement in self.import_statements:
            self.statement_sections[import_statement.category].append(import_statement)

    def sorted_import_statements(self):
        io_str = ''

        if self.statement_sections[ImportSection.SYSTEM_PACKAGE]:
            io_str += '\n'.join(
                (i.text for i in sorted(self.statement_sections[ImportSection.SYSTEM_PACKAGE], self._sort_packages))
            )
            io_str += '\n\n'
        if self.statement_sections[ImportSection.INSTALLED_PACKAGE]:
            io_str += '\n'.join(
                (i.text for i in sorted(self.statement_sections[ImportSection.INSTALLED_PACKAGE], self._sort_packages))
            )
            io_str += '\n\n'
        if self.statement_sections[ImportSection.PROJECT_PACKAGE]:
            io_str += '\n'.join(
                (i.text for i in sorted(self.statement_sections[ImportSection.PROJECT_PACKAGE], self._sort_packages))
            )
            io_str += '\n'

        return io_str.strip()

    @staticmethod
    def _sort_packages(p1, p2):
        if p1._import_type == p2._import_type:
            if p1._package_name < p2._package_name:
                return -1
            return 1

        if p1._import_type > p2._import_type:
            return -1
        return 1


def do_program():
    parser = argparse.ArgumentParser(description='Python import ordering helper')

    parser.add_argument('--start', '-s', help='project root directory for importing')
    parser.add_argument('files', nargs='*', help='files to test for ordering')

    args = parser.parse_args()

    exit_success = True
    for fn in args.files:
        if os.path.isdir(fn):
            for dirpath, dirnames, filenames in os.walk(fn):
                for f in filenames:
                    exit_success = do_program_one_file(os.path.join(dirpath, f), args.start) and exit_success
        else:
            exit_success = do_program_one_file(fn, args.start) and exit_success

    sys.exit(0 if exit_success else 1)


def do_program_one_file(filename, start_dir=None):
    if not os.path.exists(filename):
        print >> sys.stderr, 'File not found: {}'.format(filename)
        return False

    if not filename.endswith('.py'):
        return True

    with open(filename) as f:
        io = ImportOrderer(f, start=start_dir)
        io.read_file()

    io.categorize_statements()
    io_str = io.sorted_import_statements()

    with open(filename) as f:
        uo_str = f.read()

    if not uo_str.startswith(io_str):
        print '{{{}}}:'.format(filename)
        print '{}\n'.format(io_str)
        return False

    return True


if __name__ == '__main__':
    do_program()
