import os
from subprocess import Popen, PIPE
from unittest import TestCase


TESTS_ROOT = os.path.dirname(os.path.abspath(__file__))
PROJECT_ROOT = os.path.dirname(TESTS_ROOT)


class ExternalScriptTest(TestCase):
    def __init__(self, *args, **kwargs):
        super(ExternalScriptTest, self).__init__(*args, **kwargs)

        self._python_executable_path = os.path.join(PROJECT_ROOT, 'src', 'import_orderer.py')
        self._src_root = os.path.join(TESTS_ROOT, 'some_fake_project')
        self._good_src_path = os.path.join(self._src_root, 'some_fake_package', 'some_good_fake_module.py')
        self._bad_src_path = os.path.join(self._src_root, 'some_fake_package', 'some_bad_fake_module.py')

    def test_exit_success(self):
        proc = Popen(['python', self._python_executable_path, self._good_src_path], stdout=PIPE, stderr=PIPE)
        stdout, stderr = proc.communicate()

        self.assertEqual(proc.returncode, 0)
        self.assertEqual(stdout, '')
        self.assertEqual(stderr, '')

    def test_exit_failure(self):
        proc = Popen(['python', self._python_executable_path, self._bad_src_path], stdout=PIPE, stderr=PIPE)
        stdout, stderr = proc.communicate()

        self.assertEqual(proc.returncode, 1)

        with open(self._good_src_path) as f:
            self.assertEqual(stdout, '{{{}}}:\n{}\n'.format(self._bad_src_path, f.read().replace('_bad_', '_good_')))

        self.assertEqual(stderr, '')
