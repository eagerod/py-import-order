import os
from unittest import TestCase

from src.import_orderer import ImportOrderer


TESTS_ROOT = os.path.dirname(os.path.abspath(__file__))
PROJECT_ROOT = os.path.dirname(TESTS_ROOT)


class ImportOrderingTest(TestCase):
    def __init__(self, *args, **kwargs):
        super(ImportOrderingTest, self).__init__(*args, **kwargs)

        self._src_root = os.path.join(TESTS_ROOT, 'some_fake_project')
        self._good_src_path = os.path.join(self._src_root, 'some_fake_package', 'some_good_fake_module.py')
        self._bad_src_path = os.path.join(self._src_root, 'some_fake_package', 'some_bad_fake_module.py')
        self._json_src_path = os.path.join(self._src_root, 'some_fake_package', 'some_json_using_module.py')

    def test_already_ordered(self):
        with open(self._good_src_path) as f:
            io = ImportOrderer(f)
            io.read_file()

        io.categorize_statements()
        io_str = io.sorted_import_statements()

        with open(self._good_src_path) as f:
            self.assertEqual(io_str, f.read().strip())

    def test_not_ordered(self):
        with open(self._bad_src_path) as f:
            io = ImportOrderer(f)
            io.read_file()

        io.categorize_statements()
        io_str = io.sorted_import_statements()

        with open(self._good_src_path) as f:
            self.assertEqual(io_str, f.read().strip().replace('_bad_', '_good_'))

    def test_json_ordered_properly(self):
        with open(self._json_src_path) as f:
            io = ImportOrderer(f)
            io.read_file()

        io.categorize_statements()
        io_str = io.sorted_import_statements()

        with open(self._json_src_path) as f:
            self.assertEqual(io_str, f.read().strip())
